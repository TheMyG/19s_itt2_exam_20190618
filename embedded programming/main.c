/*
 * Author : henn6653
 */ 
#include <avr/io.h>
#include <stdio.h>
#include <avr/interrupt.h>
#include "uart.h"

volatile int ADCvalue = 0;
volatile float volts = 0;	
volatile char snum[10];

float convert2volts(int adcval);

int main(void)
{
	ioinit();
	
	ADMUX = 0;
	ADMUX |= (1 << REFS0);
	ADMUX &= ~(1 << ADLAR);
	ADCSRA |= (1 << ADPS2) | (1 << ADPS1) | (1 << ADPS0);
	ADCSRA |= (1 << ADATE);
	ADCSRB = 0;
	ADCSRA |= (1 << ADEN);    // Enable the ADC
	ADCSRA |= (1 << ADIE);    // Enable Interrupts
	ADCSRA |= (1 << ADSC);    
	sei();


	while (1)
	{
		volts = convert2volts(ADCvalue);
		if (volts > 4.9){
			printf("d");
		} else if (volts > 4){
			printf("w");
		} else if (volts > 3){
			printf("s");
		}
	}
}

float convert2volts(int adcval){
	float tmp = 0;
	tmp = adcval * 0.0048828125;
	return tmp;
}

ISR(ADC_vect)
{
	uint8_t tmp;            
	tmp = ADMUX;
	tmp &= 0x0F;            
	ADCvalue = ADCL;
	ADCvalue = (ADCH << 8) + ADCvalue;
}
